package com.bukuwarung.technicaltest.util;

import com.bukuwarung.technicaltest.model.BaseResponse;

public class BasicUtil {

    public static <T> BaseResponse<T> constructResponse(boolean status, String message, T data) {
        return BaseResponse.<T>builder()
                .status(status)
                .message(message)
                .data(data)
                .build();
    }
}
