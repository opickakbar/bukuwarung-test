package com.bukuwarung.technicaltest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonDeserialize(builder = PaymentRequest.PaymentRequestBuilder.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentRequest implements Serializable {

    @JsonProperty("amount")
    private Long amount;

    @Builder
    public PaymentRequest(Long amount) {
        this.amount = amount;
    }

    @JsonPOJOBuilder(withPrefix = "")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class PaymentRequestBuilder {

        @JsonProperty("amount")
        public PaymentRequestBuilder amount(Long amount) {
            this.amount = amount;
            return this;
        }
    }
}
