package com.bukuwarung.technicaltest.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class BaseResponse<T> {
    private boolean status;
    private String message;
    private T data;
}
