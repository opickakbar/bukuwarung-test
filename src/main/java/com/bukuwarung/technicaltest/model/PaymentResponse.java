package com.bukuwarung.technicaltest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Builder
@Data
public class PaymentResponse implements Serializable {

    @JsonProperty("id")
    private String id;

    @JsonProperty("amount")
    private Long amount;

}
