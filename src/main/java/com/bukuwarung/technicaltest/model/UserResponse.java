package com.bukuwarung.technicaltest.model;

import com.bukuwarung.technicaltest.dto.Payment;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Builder
@Data
public class UserResponse implements Serializable {

    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("balance")
    private Long balance;

    @JsonProperty("payments")
    private List<Payment> payments;

}
