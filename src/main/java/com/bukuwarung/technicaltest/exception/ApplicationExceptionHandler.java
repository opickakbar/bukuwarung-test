package com.bukuwarung.technicaltest.exception;

import com.bukuwarung.technicaltest.model.BaseResponse;
import com.bukuwarung.technicaltest.util.BasicUtil;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ApplicationExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public BaseResponse<String> internalServerError(Exception exception) {
        return BasicUtil.constructResponse(false, exception.getMessage(), null);
    }

    @ExceptionHandler(value = NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public BaseResponse<String> internalServerError(NotFoundException exception) {
        return BasicUtil.constructResponse(false, exception.getMessage(), null);
    }
}
