package com.bukuwarung.technicaltest.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotFoundException extends RuntimeException {
    private static final long serialVersionUID = 200L;
    private String processId;
    private String responseLog;
    public NotFoundException(String message) {
        super(message);
    }
    public NotFoundException(String message, String responseLog) {
        super(message);
        this.responseLog = responseLog;
    }
}
