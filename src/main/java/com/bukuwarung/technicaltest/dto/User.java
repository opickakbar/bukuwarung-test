package com.bukuwarung.technicaltest.dto;

import com.bukuwarung.technicaltest.util.StringListConverter;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "users")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "balance")
    private Long balance;

    @Column(name = "payment_ids", insertable = false, updatable = false)
    @Convert(converter = StringListConverter.class)
    private List<String> paymentIds;

    @Column(name = "payment_ids")
    private String paymentIdsString;
}
