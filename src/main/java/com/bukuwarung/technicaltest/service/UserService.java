package com.bukuwarung.technicaltest.service;

import com.bukuwarung.technicaltest.model.BaseResponse;
import com.bukuwarung.technicaltest.model.UserResponse;

public interface UserService {
    BaseResponse<UserResponse> getUser (String id);
}
