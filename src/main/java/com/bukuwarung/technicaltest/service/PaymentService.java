package com.bukuwarung.technicaltest.service;

import com.bukuwarung.technicaltest.model.BaseResponse;
import com.bukuwarung.technicaltest.model.PaymentRequest;
import com.bukuwarung.technicaltest.model.PaymentResponse;

public interface PaymentService {
    BaseResponse<PaymentResponse> doPayment (String userId, PaymentRequest paymentRequest);
}
