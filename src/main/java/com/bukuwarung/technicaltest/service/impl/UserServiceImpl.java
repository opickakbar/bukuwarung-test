package com.bukuwarung.technicaltest.service.impl;

import com.bukuwarung.technicaltest.constant.MessageConstant;
import com.bukuwarung.technicaltest.dto.Payment;
import com.bukuwarung.technicaltest.dto.User;
import com.bukuwarung.technicaltest.exception.NotFoundException;
import com.bukuwarung.technicaltest.model.BaseResponse;
import com.bukuwarung.technicaltest.model.UserResponse;
import com.bukuwarung.technicaltest.repository.PaymentRepository;
import com.bukuwarung.technicaltest.repository.UserRepository;
import com.bukuwarung.technicaltest.service.UserService;
import com.bukuwarung.technicaltest.util.BasicUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PaymentRepository paymentRepository;

    @Override
    public BaseResponse<UserResponse> getUser(String uuid) {
        User user = Optional.ofNullable(userRepository.findOne(uuid))
                .orElseThrow(() -> new NotFoundException(MessageConstant.USER_NOT_FOUND));
        List<String> paymentIds = user.getPaymentIds();
        List<Payment> paymentList = paymentRepository.findByPaymentIds(paymentIds);
        UserResponse userResponse = constructUserResponse (user, paymentList);
        return BasicUtil.constructResponse(true, "Success get user", userResponse);
    }

    private UserResponse constructUserResponse (User user, List<Payment> paymentList) {
        return UserResponse.builder()
                .id(user.getId())
                .name(user.getName())
                .balance(user.getBalance())
                .payments(paymentList)
                .build();
    }
}
