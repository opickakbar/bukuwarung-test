package com.bukuwarung.technicaltest.service.impl;

import com.bukuwarung.technicaltest.constant.MessageConstant;
import com.bukuwarung.technicaltest.dto.Payment;
import com.bukuwarung.technicaltest.dto.User;
import com.bukuwarung.technicaltest.exception.NotFoundException;
import com.bukuwarung.technicaltest.model.BaseResponse;
import com.bukuwarung.technicaltest.model.PaymentRequest;
import com.bukuwarung.technicaltest.model.PaymentResponse;
import com.bukuwarung.technicaltest.repository.PaymentRepository;
import com.bukuwarung.technicaltest.repository.UserRepository;
import com.bukuwarung.technicaltest.service.PaymentService;
import com.bukuwarung.technicaltest.util.BasicUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PaymentRepository paymentRepository;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public BaseResponse<PaymentResponse> doPayment(String userId, PaymentRequest paymentRequest) {
        User userData = Optional.ofNullable(userRepository.findOne(userId))
                .orElseThrow(() -> new NotFoundException(MessageConstant.USER_NOT_FOUND));
        Payment newPayment = constructNewPayment(paymentRequest);
        paymentRepository.save(newPayment);
        String paymentList = userData.getPaymentIdsString();
        if (Objects.isNull(paymentList)) {
            userData.setPaymentIdsString(newPayment.getId());
        } else {
            userData.setPaymentIdsString(paymentList.concat(";").concat(newPayment.getId()));
        }
        userData.setBalance(userData.getBalance() + newPayment.getAmount());
        userRepository.save(userData);
        return BasicUtil.constructResponse(true, "Success do payment", PaymentResponse.builder()
                .id(newPayment.getId())
                .amount(paymentRequest.getAmount())
                .build());
    }

    private Payment constructNewPayment (PaymentRequest paymentRequest) {
        return Payment.builder()
                .id(UUID.randomUUID().toString())
                .amount(paymentRequest.getAmount())
                .build();
    }
}
