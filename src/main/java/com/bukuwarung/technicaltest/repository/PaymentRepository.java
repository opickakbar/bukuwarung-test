package com.bukuwarung.technicaltest.repository;

import com.bukuwarung.technicaltest.dto.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, String> {

    @Query("SELECT p FROM Payment p WHERE p.id IN (:idList)")
    List<Payment> findByPaymentIds(@Param("idList") List<String> idList);
}
