package com.bukuwarung.technicaltest.controller;

import com.bukuwarung.technicaltest.model.BaseResponse;
import com.bukuwarung.technicaltest.model.PaymentRequest;
import com.bukuwarung.technicaltest.model.PaymentResponse;
import com.bukuwarung.technicaltest.model.UserResponse;
import com.bukuwarung.technicaltest.service.PaymentService;
import com.bukuwarung.technicaltest.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
public class ApplicationController {

    @Autowired
    private UserService userService;

    @Autowired
    private PaymentService paymentService;

    @GetMapping("/user/{userId}")
    public BaseResponse<UserResponse> getUser(@PathVariable String userId) {
        return userService.getUser(userId);
    }

    @PostMapping("/user/{userId}/payments")
    @Transactional(readOnly = true)
    public BaseResponse<PaymentResponse> userPay(@PathVariable(value = "userId") String userId, @RequestBody PaymentRequest paymentRequest) {
        try {
            return paymentService.doPayment(userId, paymentRequest);
        } catch (ObjectOptimisticLockingFailureException e) {
            log.warn("The userId: {} is being used by the other person, will try once again..", userId);
            return paymentService.doPayment(userId, paymentRequest);
        }
    }

}
